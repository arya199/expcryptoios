//
//  main.m
//  ExpCrypto
//
//  Created by Rhama Arya Wibawa on 8/16/12.
//  Copyright (c) 2012 Rhama Arya Wibawa. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SCAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([SCAppDelegate class]));
    }
}
