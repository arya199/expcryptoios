//
//  ExpAppDelegate.h
//  ExpCrypto
//
//  Created by Rhama Arya Wibawa on 8/15/12.
//  Copyright (c) 2012 Rhama Arya Wibawa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ExpAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
