//
//  SCCryptoTest.m
//  ExpCrypto
//
//  Created by Rhama Arya Wibawa on 8/16/12.
//  Copyright (c) 2012 Rhama Arya Wibawa. All rights reserved.
//

#import "SCCryptoTest.h"
#import "FBEncryptorAES.h"
#import "NSData+Base64.h"

const NSString *AES_KEY = @"oPTsCSpBR13CDoA4";

@implementation SCCryptoTest {
    NSData *iv;
    NSMutableData *sentIv;
    NSData *sentIv2;
}

- (void)dumping {
    
}

- (void)encryptAesCbc:(NSString *)source {
    NSLog(@"AES_KEY: %s", [AES_KEY UTF8String]);
    NSLog(@"%s", [source UTF8String]);
    
    NSData *bytes = [AES_KEY dataUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"%u", [bytes length]);
    
    NSData *encrypt = [FBEncryptorAES encryptData:[source dataUsingEncoding:NSUTF8StringEncoding] key:[AES_KEY dataUsingEncoding:NSUTF8StringEncoding] iv:iv];
    
    NSLog(@"%u", [encrypt length]);
    NSLog(@"encrypted: %@", [encrypt base64EncodedString]);
    NSLog(@"with iv: %@", [iv base64EncodedString]);
    NSLog(@"and appended iv: %@", [sentIv base64EncodedString]);
}

- (void)encryptAesWith64Thingy:(NSString *)source {
    NSString *encrypt = [FBEncryptorAES encryptBase64String:source keyString:AES_KEY separateLines:NO];
    
    NSLog(@"encrypted: %@", encrypt);
}

- (void)decryptAesCbc:(NSString *)source withIv:(NSString *)ivMethod {
    NSData *initialData = [NSData dataFromBase64String:ivMethod];
    initialData = [initialData subdataWithRange:NSMakeRange(2, 16)];
    const char *bytes = [initialData bytes];
    NSMutableData *mutableData = [[NSMutableData alloc] initWithCapacity:16];
    
    [mutableData replaceBytesInRange:NSMakeRange(0, [initialData length]) withBytes:bytes];
    const char *newBytes = [mutableData bytes];
    for (int i = 0; i < [mutableData length]; i++) {
        NSLog(@"%u. %hhd", i, (unsigned char)newBytes[i]);
    }
    NSData *data = [FBEncryptorAES decryptData:[NSData dataFromBase64String:source] key:[AES_KEY dataUsingEncoding:NSUTF8StringEncoding] iv:mutableData];
    NSLog(@"decrypted: %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
}

- (void)decryptAesWith64Thingy:(NSString *)source {
    NSString *result = [FBEncryptorAES decryptBase64String:source keyString:AES_KEY];
    NSLog(@"decrypted with 64 thingy: %@", result);
}

- (void)generateIv {
    iv = [FBEncryptorAES generateIv];
    sentIv = [[NSMutableData alloc] initWithCapacity:18];
    sentIv2 = [NSData dataFromBase64String:[FBEncryptorAES hexStringForData:iv]];
    const char *newBytes2 = [sentIv bytes];
    const char *bytes = [iv bytes];
    char constantBytes[2] = {4, 16};
    [sentIv appendBytes:constantBytes length:2];
    
    [sentIv replaceBytesInRange:NSMakeRange(2, 16) withBytes:bytes];
    const char *newBytes = [sentIv bytes];
    
    for (int i = 0; i < [sentIv length]; i++) {
        NSLog(@"%u. %hhd", i, (unsigned char)newBytes[i]);
    }
    
    for (int i = 0; i < [sentIv2 length]; i++) {
        NSLog(@"%u. %hhd", i, (unsigned char)newBytes2[i]);
    }
}

@end
