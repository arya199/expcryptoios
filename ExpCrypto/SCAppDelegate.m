//
//  SCAppDelegate.m
//  ExpCrypto
//
//  Created by Rhama Arya Wibawa on 8/16/12.
//  Copyright (c) 2012 Rhama Arya Wibawa. All rights reserved.
//

#import "SCAppDelegate.h"
#import "SCCryptoTest.h"

@implementation SCAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    
    SCCryptoTest *cryptoTest = [[SCCryptoTest alloc] init];
    
    [cryptoTest dumping];
    
    [cryptoTest generateIv];
    [cryptoTest encryptAesCbc:@"aa2cc84b-7ed1-4429-88b1-c6e4b81026bc"];
//    [cryptoTest encryptAesWith64Thingy:@"ea1cdc0d-5dcb-4c92-93b6-664c649c831d"];
//    [cryptoTest decryptAesWith64Thingy:@"JHrLJyKie7aoHkh26y84dxXpmqqEnpp5mAupbf9JqHgsVapfmIlrKEuwrD74FE+r"];
//    [cryptoTest decryptAesCbc:@"JHrLJyKie7aoHkh26y84dxXpmqqEnpp5mAupbf9JqHgsVapfmIlrKEuwrD74FE+r" withIv:@"BBBrJeKEnjOLaafiEgh3wu7r"];
    
    return YES;
}

@end
