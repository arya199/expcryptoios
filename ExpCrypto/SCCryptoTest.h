//
//  SCCryptoTest.h
//  ExpCrypto
//
//  Created by Rhama Arya Wibawa on 8/16/12.
//  Copyright (c) 2012 Rhama Arya Wibawa. All rights reserved.
//

#import <Foundation/Foundation.h>

const extern NSString *AES_KEY;

@interface SCCryptoTest : NSObject

- (void)generateIv;
- (void)encryptAesCbc:(NSString *)source;
- (void)encryptAesWith64Thingy:(NSString *)source;
- (void)decryptAesCbc:(NSString *)source withIv:(NSString *)ivMethod;
- (void)decryptAesWith64Thingy:(NSString *)source;

- (void)dumping;

@end
